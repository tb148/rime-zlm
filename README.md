# rime-zlm

This is a zbalermorna IME base on rime.

Type any consonant to insert the zbalermorna equivalent. Lowercase vowels are inserted as diacritics, unless they form semivowels. Uppercase vowels form full vowels. All diphthongs are automatic. `h` or `'` will insert a yhybu, and `.` will insert denpabu. The attitudinal shorthand is created automatically with ligatures.

`,` will separate two full vowels from becoming a diphthong, or prevent a vowel from being self-dotted with a denpabu.

`-` inserts one character of silence.

`~` applies stretch to the previous character - use after the vowel.

`1234` adds the respective intonation to the previous character - use after the vowel.

`` ` `` adds dynamics to the previous character - use after the vowel.
